FROM php:8.1 AS base

# Define a separate stage for application build
WORKDIR /app
COPY . .

RUN apt-get update -yqq && apt-get install -yqq --no-install-recommends \
  git \
  libpq-dev \
  libcurl4-gnutls-dev \
  libicu-dev \
  libvpx-dev \
  libjpeg-dev \
  libpng-dev \
  libxpm-dev \
  zlib1g-dev \
  libfreetype6-dev \
  libxml2-dev \
  libexpat1-dev \
  libbz2-dev \
  libgmp3-dev \
  libldap2-dev \
  unixodbc-dev \
  libsqlite3-dev \
  libaspell-dev \
  libsnmp-dev \
  libpcre3-dev \
  libtidy-dev \
  libonig-dev \
  libzip-dev

# Install PHP extensions
RUN docker-php-ext-install mbstring pdo_mysql curl intl gd xml zip bz2 opcache

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN php composer.phar install

# Define a separate stage for Apache configuration
FROM php:8.2-apache AS web

# Copy virtual host configuration
COPY deploy/deploy.conf /etc/apache2/sites-available/deploy.conf
RUN a2ensite deploy.conf && a2dissite 000-default.conf

# Copy application to document root within the virtual host
COPY --from=base /app/ /var/www/html/

# Expose the port
EXPOSE 80

# Final stage combines base and web stages
FROM web AS final
